Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ayatana-indicator-display
Upstream-Contact: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
Source: https://github.com/AyatanaIndicators/ayatana-indicator-display

Files: po/*.po
 po/ayatana-indicator-display.pot
Copyright: 2017, THE PACKAGE'S COPYRIGHT HOLDER
  YEAR, THE PACKAGE'S COPYRIGHT HOLDER
License: GPL-3

Files: .build.yml
 .travis.yml
 AUTHORS
 CMakeLists.txt
 ChangeLog
 INSTALL.md
 NEWS
 README
 README.md
 data/50-org.ayatana.indicator.display.AccountsService.pkla
 data/50-org.ayatana.indicator.display.AccountsService.rules
 data/CMakeLists.txt
 data/ayatana-indicator-display.conf.in
 data/ayatana-indicator-display.desktop.in
 data/ayatana-indicator-display.service.in
 data/icons/ayatana-indicator-display-brightness-high.svg
 data/icons/ayatana-indicator-display-brightness-low.svg
 data/icons/ayatana-indicator-display-colortemp-off.svg
 data/icons/ayatana-indicator-display-colortemp-on.svg
 data/org.ayatana.indicator.display
 data/org.ayatana.indicator.display.AccountsService.policy
 data/org.ayatana.indicator.display.AccountsService.xml
 data/org.ayatana.indicator.display.gschema.xml
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 src/CMakeLists.txt
 tests/CMakeLists.txt
 tests/manual
 tests/unit/CMakeLists.txt
Copyright: 2013-2016, Canonical Ltd.
  2021-2024, Robert Tari <robert@tari.in>
License: GPL-3
Comment:
 Assuming same license and copyright holdership as found in the
 project files containing a license header.

Files: src/dbus-names.h
 src/exporter.h
 src/indicator.cpp
 tests/unit/rotation-lock-test.cpp
 tests/utils/glib-fixture.h
 tests/utils/test-dbus-fixture.h
Copyright: 2013, Canonical Ltd.
  2014, Canonical Ltd.
  2016, Canonical Ltd.
License: GPL-3

Files: src/exporter.cpp
 src/indicator.h
 src/main.cpp
 src/service.cpp
 src/service.h
Copyright: 2014, Canonical Ltd.
  2014-2016, Canonical Ltd.
  2022-2023, Robert Tari
  2023, Robert Tari
License: GPL-3

Files: src/solar.c
 src/solar.h
Copyright: 2010, Jon Lund Steffensen
  2023, Robert Tari
License: GPL-3

Files: update-po.sh
 update-pot.sh
Copyright: 2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: debian/*
Copyright: 2014, Canonical Ltd.
  2017-2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
